import java.util.ArrayList;
import java.util.Arrays;

public class ClassOne {

    void graphMaker() {
   /*     ArrayList<String>vertexList=new ArrayList<>();
        vertexList.add("A");
        vertexList.add("B");
        vertexList.add("C");
        vertexList.add("D");
        vertexList.add("E");
     */
        int[][] graph = {{0,20, 10, 0, 0}, {20, 0, 0, 150, 80}, {10, 0, 0, 20, 0}, {0, 150, 20, 0, 90}, {0, 80, 0, 90, 0}};
        for (int[] ints : graph) {
            System.out.println(Arrays.toString(ints));
        }
    }
}

class ClassOneMain {
    public static void main(String[] args) {
        ClassOne classOne = new ClassOne();
        classOne.graphMaker();
    }
}