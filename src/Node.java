//Här bygger vi en egen datastruktur  av objekt!!! coolt som fan! //Khazar

//insert and show method.
//Insert metod insert a new node as next node and includes the data and weight as values in the node
class Main {
    public static void main(String[] args) {
        String[] vertexList = {"A", "B", "C", "D", "E","F","G","H"};
        int[] weightList = {20, 30, 15, 40, 10};
        LinkedList list = new LinkedList();
        list.insert(vertexList[1], weightList[1]);
        list.insert(vertexList[0],weightList[0]);
        list.insert(vertexList[2],weightList[2]);



        /*for (int j = 0; j < vertexList.length; j++) {
            list.insert(vertexList[j], weightList[j]);
        }*/
        //     list.insertAtStart("Z", 90);
        ///   list.deleteAt(0);
        list.show();


    }
}

public class Node {
    String data; //declare som data
    int weight; //declare some more data
    Node nextNode; //next is a Node

}

class LinkedList {
    private Node head; //declare head as a Node

    public void insert(String data, int weight) {
        Node node = new Node(); //Make a object
        node.data = data; // init. the incoming data as node.data as a parameter in Node
        node.weight = weight; //Init. the incoming data as node.weight as a parameter in Node.
        node.nextNode = null; //Init. new Node
        if (head == null) { //If no Nodes, declare a new Node called head
            head = node;
        } else { //if a Node exists n is head of nodes and while there is a node add a new node
            Node n = head;
            while (n.nextNode != null) {
                n = n.nextNode;
            }
            n.nextNode = node;

        }
    }

    public void insertAtStart(String data, int weight) { //Inits head as a new Node and adds to the first place in list
        Node newNode = new Node();
        newNode.data = data;
        newNode.weight = weight;
        newNode.nextNode = null;
        newNode.nextNode = head;
        head = newNode;
    }

    public void inserAtOptionalIndex(int index, String data, int weight) {
        Node newNode = new Node();
        newNode.data = data;
        newNode.weight = weight;
        newNode.nextNode = null;
        Node n = head;
        if (index == 0) {
            insertAtStart(data, weight);
        } else {
            for (int i = 0; i < index - 1; i++) {
                n = n.nextNode;
            }
            newNode.nextNode = n.nextNode;
            n.nextNode = newNode;
        }
    }

    void deleteAt(int index) {
        if (index == 0) {
            head = head.nextNode;
        } else {
            Node n = head;
            for (int i = 0; i < index - 1; i++) {
                n = n.nextNode;
            }
            n = n.nextNode.nextNode;
        }
    }

    //Show loops throw the nodes untill null and prints out (even the null node)
    public void show() {
        Node node = head;
        while (node.nextNode != null) {
            System.out.println(node.data + " " + node.weight);
            node = node.nextNode;
        }
        System.out.println(node.data + " " + node.weight);
    }

}
