import java.util.LinkedList;

public class WeightedGraph {
    static class Edge {
        int source;
        int destination;
        int weight;

        public Edge(int source, int destination, int weight) {
            this.source = source;
            this.destination = destination;
            this.weight = weight;
        }
    }

    static class Graph {
        int vertices;
        LinkedList<Edge>[] adjacencylist;

        Graph(int vertices) {
            this.vertices = vertices;
            adjacencylist = new LinkedList[vertices];
            for (int i = 0; i < vertices; i++) {
                adjacencylist[i] = new LinkedList<>();
            }
        }

        public void addEdge(int source, int destination, int weight) {
            Edge edge = new Edge(source, destination, weight);
            //Adding edge
            adjacencylist[source].addFirst(edge);
            //suppose to Add back edge
            adjacencylist[destination].add(edge);

        }

        public void printGraph() {
            for (int i = 0; i < vertices; i++) {
                LinkedList<Edge> list = adjacencylist[i];
                for (Edge edge : list) {
                    System.out.println("Vertex " + i + " is connected to: " + edge.destination + " and weight is " + edge.weight);
                }
                System.out.println();
            }
        }
    }

    public static void main(String[] args) {
        int v = 10;
        Graph graph = new Graph(v);
        graph.addEdge(0, 1, 10);
        graph.addEdge(0, 3, 20);
        graph.addEdge(1, 2, 5);
        graph.addEdge(1, 7, 30);
        graph.addEdge(2, 3, 15);
        graph.addEdge(4, 5, 50);
        graph.addEdge(4, 6, 40);
        graph.addEdge(5, 8, 25);
        graph.addEdge(6, 7, 35);
        graph.addEdge(7, 9, 45);
        graph.addEdge(8, 9, 60);
        graph.printGraph();
    }

}
